const getCube = 2**3;
console.log(`The cube of 2 is ${getCube}`);

const address = ['258', 'Washington Ave NW', 'California', '90011'];

const [house_number, street, state, ZIP_code] = address;
console.log(`I live at ${house_number} ${street} ${state} ${ZIP_code}`);

const animal ={
	name: 'Lolong',
	category: 'saltwater crocodile',
	weight: '1075 kgs',
	measurement: '20 ft 3 in'
};

const {name, category, weight, measurement} = animal;
console.log(`${name} was a ${category}. He weighed at ${weight} with a measurement of ${measurement}.`);

const numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => {
	console.log(number);
});

const reduceNumber = numbers.reduce((accumulator, currentValue) => accumulator + currentValue);
console.log(reduceNumber);